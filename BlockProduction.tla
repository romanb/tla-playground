---------------------------- MODULE BlockProduction ----------------------------
(******************************************************************************)
(* A high-level specification of what a block production scheme with          *)
(* eventual consensus is supposed to achieve.                                 *)
(******************************************************************************)
EXTENDS Integers, Sequences, FiniteSets

CONSTANTS Nodes, Nil, BestChain(_)
VARIABLES blocks

ASSUME \A n \in Nodes: n # Nil

-------------------------------------------------------------------------------
(******************************************************************************)
(* A minimal specification of a block tree.                                   *)
(******************************************************************************)

BlockNr == Nat

BlockHash == Nat

Block ==
    [ nr     : BlockNr
    , hash   : BlockHash
    , parent : BlockHash
    , author : Nodes \cup {Nil}
    ]

Genesis ==
    [ nr     |-> 1
    , hash   |-> 1
    , parent |-> 0
    , author |-> Nil
    ]

(******************************************************************************)
(* The uniquely determined parent block of a given block b.                   *)
(******************************************************************************)
Parent(b) == CHOOSE p \in blocks[b.author]: p.hash = b.parent

(******************************************************************************)
(* The set of blocks known to all Nodes.                                      *)
(******************************************************************************)
AllBlocks == UNION { blocks[n]: n \in Nodes }

(******************************************************************************)
(* Choose the block b from a set of blocks s that satifies the                *)
(* predicate P(b, c) for all blocks c in s.                                   *)
(*                                                                            *)
(* If such a block is not uniquely determined, the choice is fixed among      *)
(* the candidates but unspecified, as per the semantics of CHOOSE.            *)
(******************************************************************************)
ChooseBlockBy(P(_,_), s) == CHOOSE b \in s: \A c \in s: P(b, c)

MaxBlockByNr(s) == ChooseBlockBy(LAMBDA b1, b2: b1.nr >= b2.nr, s)

MaxBlockNr(s) == MaxBlockByNr(s).nr

MaxBlockByHash(s) == ChooseBlockBy(LAMBDA b1, b2: b1.hash >= b2.hash, s)

MaxBlockHash(s) == MaxBlockByHash(s).hash

(******************************************************************************)
(* The set of children of a block b known to validator v.                     *)
(******************************************************************************)
Children(v, b) == {c \in blocks[v]: c.parent = b.hash}

(******************************************************************************)
(* The chain (sequence) of blocks from one block to another.                  *)
(*                                                                            *)
(* If the blocks are not on the same chain, this is the empty sequence.       *)
(******************************************************************************)
RECURSIVE Chain(_,_)
Chain(from, to) ==
    CASE from.hash = to.hash -> <<from>>
      [] to.nr <= from.nr -> <<>>
      [] \E p \in blocks[to.author]: p.hash = to.parent ->
            Append(Chain(from, Parent(to)), to)
      [] OTHER -> <<>>

(******************************************************************************)
(* The range of a function.                                                   *)
(******************************************************************************)
Range(f) == { f[x]: x \in DOMAIN f }

(******************************************************************************)
(* The set of blocks on the chain from Genesis to b, i.e. all ancestors.      *)
(******************************************************************************)
Ancestors(b) == Range(Chain(Genesis, b))

(******************************************************************************)
(* Whether b1 is an ancestor of (or equal to) b2 in the block tree,           *)
(* defining a partial order on blocks.                                        *)
(******************************************************************************)
RECURSIVE _ \preceq _
b1 \preceq b2 ==
    CASE b1.hash = b2.hash -> TRUE
      [] b1.hash = b2.parent -> TRUE
      [] b1.nr > b2.nr -> FALSE
      [] OTHER -> b1 \preceq Parent(b2)

b1 \prec b2 == b1 \preceq b2 /\ b1 # b2

b1 \succeq b2 == b2 \preceq b1

b1 \succ b2 == b1 \succeq b2 /\ b1 # b2

b1 \sim b2 == b1 \preceq b2 \/ b2 \preceq b1

Last(s) == LET l == Len(s) IN s[l]

(******************************************************************************)
(* A simple fork-choice rule that picks the longest chain. This operator      *)
(* can be substituted for `BestChain(_)' in a model.                          *)
(******************************************************************************)
LongestChain(v) == MaxBlockByNr(blocks[v])

-------------------------------------------------------------------------------
(******************************************************************************)
(* Protocol Actions                                                           *)
(******************************************************************************)

NewBlock(n) ==
    LET
    p == BestChain(n)
    b == [ nr     |-> p.nr + 1
         , hash   |-> MaxBlockHash(AllBlocks) + 1
         , parent |-> p.hash
         , author |-> n
         ]
    IN
    blocks' = [blocks EXCEPT ![n] = @ \cup {b}]

RecvBlock(n) ==
    \E m \in Nodes:
        \E b \in blocks[m]:
            /\ b \notin blocks[n]
            /\ blocks' = [blocks EXCEPT ![n] = @ \cup Ancestors(b)]

-------------------------------------------------------------------------------
(******************************************************************************)
(* Full specification.                                                        *)
(******************************************************************************)

Init == blocks = [n \in Nodes |-> {Genesis}]

Next == \E n \in Nodes: NewBlock(n) \/ RecvBlock(n)

Spec == Init /\ [][Next]_blocks /\ WF_blocks(Next)

TypeOK == blocks \in [Nodes -> SUBSET Block]

THEOREM Spec => []TypeOK

(******************************************************************************)
(* All nodes agree on the best chain.                                         *)
(******************************************************************************)
Consensus == \A n1, n2 \in Nodes: BestChain(n1) \sim BestChain(n2)

(******************************************************************************)
(* The nodes always eventually (i.e. infinitely often) agree on the           *)
(* best chain.                                                                *)
(******************************************************************************)
THEOREM Spec => []<>Consensus

================================================================================