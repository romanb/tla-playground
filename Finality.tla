------------------------------ MODULE Finality ------------------------------
(******************************************************************************)
(* A high-level specification of what block finality consensus is supposed    *)
(* to achieve.                                                                *)
(******************************************************************************)
EXTENDS Integers, Sequences, FiniteSets

CONSTANTS Validators, Nil, BestChain(_)
VARIABLES blocks, final

-------------------------------------------------------------------------------
(******************************************************************************)
(* Instantiate the high-level specification for block production, on which    *)
(* finality consensus is built.                                               *)
(******************************************************************************)
BP == INSTANCE BlockProduction WITH Nodes <- Validators

b1 \preceq b2 == BP!\preceq(b1, b2)
b1 \sim b2 == BP!\sim(b1, b2)
b1 \succ b2 == BP!\succ(b1, b2)
Last(s) == BP!Last(s)
Chain(a,b) == BP!Chain(a,b)
Block == BP!Block
MaxBlockNr(s) == BP!MaxBlockNr(s)
Genesis == BP!Genesis

-------------------------------------------------------------------------------
(******************************************************************************)
(* The head of the longest chain descending from b, with ties broken through  *)
(* a fixed but unspecified choice, as per the semantics of CHOOSE.            *)
(*                                                                            *)
(* This operator can be substituted for `BestChain(_)'.  In practice, the     *)
(* definition of "best chain" is up to the concrete block production          *)
(* mechanism underlying the finality gadget.                                  *)
(******************************************************************************)
LongestChainFrom(v, b) == BP!MaxBlockByNr({c \in blocks[v]: b \preceq c})

(******************************************************************************)
(* The head of the longest chain descending from the last finalised block     *)
(* of v. This operator can be substituted for `BestChain(_)'.                 *)
(******************************************************************************)
LongestChainFromFinal(v) == LongestChainFrom(v, Last(final[v]))

(******************************************************************************)
(* Whether validator v has finalised block b.                                 *)
(******************************************************************************)
\* IsFinal(v, b) == b \preceq Last(final[v])

-------------------------------------------------------------------------------
(******************************************************************************)
(* Protocol Actions                                                           *)
(******************************************************************************)

NewBlock(v) == BP!NewBlock(v) /\ UNCHANGED final

RecvBlock(v) == BP!RecvBlock(v) /\ UNCHANGED final

(******************************************************************************)
(* Whenever a validator v has a new block that is a descendant of its         *)
(* last finalised block and on the same chain as the finalised blocks         *)
(* of all other Validators, it can be finalised.                              *)
(******************************************************************************)
Finalise(v) ==
    \E b \in blocks[v]:
        /\ b \succ Last(final[v])
        /\ \A w \in Validators \ {v}:
            /\ b \sim Last(final[w])
            /\ LET c == Tail(Chain(Last(final[v]), b))
                IN
                /\ final' = [final  EXCEPT ![v] = @ \o c]
                /\ UNCHANGED <<blocks>>

-------------------------------------------------------------------------------
(******************************************************************************)
(* The (safety) specifications.                                               *)
(******************************************************************************)

Init ==
    /\ blocks = [v \in Validators |-> {Genesis}]
    /\ final  = [v \in Validators |-> <<Genesis>>]

Next ==
    \E v \in Validators:
        \/ NewBlock(v)
        \/ RecvBlock(v)
        \/ Finalise(v)

Spec == Init /\ [][Next]_<<blocks, final>>

(******************************************************************************)
(* The type invariant.                                                        *)
(******************************************************************************)
TypeOK ==
    /\ blocks \in [Validators -> SUBSET Block]
    /\ final \in [Validators -> Seq(Block)]

(******************************************************************************)
(* Safety: The chain of finalised blocks of each validator grows              *)
(* monotonically.                                                             *)
(******************************************************************************)
Monotonicity ==
    \A v \in Validators:
        Last(final[v]) \preceq Last(final'[v])

(******************************************************************************)
(* Safety: All Validators share a common prefix of finalised blocks, i.e.     *)
(* the heads of all finalised chains are on the same chain.                   *)
(******************************************************************************)
CommonPrefix ==
    \A v1, v2 \in Validators:
        Last(final[v1]) \sim Last(final[v2])

(******************************************************************************)
(* Safety: The sequence of finalised blocks of validator v is a valid chain.  *)
(******************************************************************************)
ValidChain(v) == Chain(Genesis, Last(final[v])) = final[v]

(******************************************************************************)
(* Safety: All Validators have a valid chain of finalised blocks.             *)
(******************************************************************************)
ValidChains == \A v \in Validators: ValidChain(v)

THEOREM Spec => []TypeOK
THEOREM Spec => []CommonPrefix
THEOREM Spec => []ValidChains
THEOREM Spec => [][Monotonicity]_final

-------------------------------------------------------------------------------
(******************************************************************************)
(* The liveness specification.                                                *)
(******************************************************************************)
LiveSpec == Spec /\ WF_<<blocks, final>>(Next)

(******************************************************************************)
(* Liveness: Whenever the height of the block tree grows, the sequence        *)
(* of finalised blocks eventually reaches a length matching the new height,   *)
(* i.e. a block is eventually finalised at every level of the block tree.     *)
(******************************************************************************)
EventualFinality ==
    \A v \in Validators:
        MaxBlockNr(blocks[v]) > Len(final[v])
            ~> MaxBlockNr(blocks[v]) = Len(final[v])

THEOREM LiveSpec => EventualFinality

===============================================================================