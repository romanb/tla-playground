------------------------------ MODULE Grandpa ------------------------------
(******************************************************************************)
(* TLA+ specification of the `GRANDPA'[1] protocol for "provable", byzantine  *)
(* fault-tolerant block finality decoupled from a concurrent block production *)
(* scheme, a type of protocol sometimes referred to as a "finality gadget".   *)
(* A high-level introduction can be found at [2] and a reference              *)
(* implementation exists at [3].  This TLA+ specification is a transcription  *)
(* of the (optimized) protocol for the partially synchronous network model,   *)
(* as it is described at the end of [1], with notable differences mentioned   *)
(* below.  In the context of this specification, participants of the protocol *)
(* are referred to as validators or voters.                                   *)
(*                                                                            *)
(* `^\tiny{ [1]                                                               *)
(* https://github.com/w3f/consensus/blob/master/pdf/grandpa.pdf}\hfill\break  *)
(* [2]                                                                        *)
(* https://medium.com/polkadot-network/grandpa-block-finality-in-polkadot-an-introduction-part-1-d08a24a021b5}\hfill\break *)
(* [3] https://github.com/paritytech/finality-grandpa\hfill\break }^'         *)
(*                                                                            *)
(* `^\large{\textbf{Protocol Overview}}^'                                     *)
(*                                                                            *)
(* The protocol progresses in rounds, with each round consisting of two       *)
(* phases in which the honest participants come to agreement on a             *)
(* monotonically growing common prefix of blocks to finalise.  In each phase, *)
(* a vote for a block is also a vote for all ancestors of that block.  A      *)
(* block is said to achieve a supermajority if it accumulates at least 2/3 of *)
(* all votes.  Since a vote for a block b counts as a vote for all blocks in  *)
(* the chain with head b, more than one block can achieve a supermajority,    *)
(* and the protocol tries to establish agreement on the block with the        *)
(* highest block number having such a supermajority.                          *)
(*                                                                            *)
(* In the first phase, every validator proposes a chain to finalise by        *)
(* sending a `prevote' for the head of its best chain containing the previous *)
(* round estimate (and thus the last finalised block) to all other            *)
(* validators.  A round estimate for a round r is the block with the highest  *)
(* block number that could have been finliased in round r and defined to be   *)
(* Genesis for round 1.                                                       *)
(*                                                                            *)
(* In the second phase, every validator receives the proposals from all       *)
(* others and broadcasts a `precommit' for the head of the "heaviest"         *)
(* subtree, represented by the block that achieved a supermajority of votes   *)
(* in the first phase, called the 2/3-prevote-GHOST block - where GHOST       *)
(* stands for Greedy Heaviest Observed Subtree, a more general approach to a  *)
(* fork-choice rule / chain selection.                                        *)
(*                                                                            *)
(* The block with the highest block number that achieved a supermajority of   *)
(* votes in the precommit phase is the head of the 2/3-precommit-GHOST.  The  *)
(* precommit-GHOST can be finalised by a validator at any time after the      *)
(* precommit phase if it is a descendant of the last finalised block and an   *)
(* ancestor of (or equal to) the head of the prevote-GHOST.  The protocol can *)
(* thus establish agreement on the finalisation of any number of blocks in    *)
(* each round among the honest validators, tolerating f < 1/3 byzantine       *)
(* participants.                                                              *)
(*                                                                            *)
(* The next round can start when the current round is completable, which is a *)
(* condition arising for the majority of honest validators some time after    *)
(* the precommit phase, when enough precommits have been observed to          *)
(* determine the round estimate.                                              *)
(*                                                                            *)
(* Prevotes on different blocks naturally occur as a result of different      *)
(* validators seeing different blocks at the head of their best chain,        *)
(* whereas precommits on different blocks occur only if not all honest        *)
(* validators receive all prevotes from all other honest validators within a  *)
(* certain time window (see the section on liveness and the conditions on the *)
(* precommit action for details).                                             *)
(*                                                                            *)
(* It may happen that a round results in the honest validators agreeing only  *)
(* on the last finalised block, if most validators vote for different chains  *)
(* whose only common ancestor is the last finalised block, which is thus the  *)
(* only block with a supermajority of votes.  However, during such a round    *)
(* each validator learns about the chains of the others through the votes     *)
(* received, and since all honest validators are assumed to use the same      *)
(* fork-choice rule for the underlying block production scheme (e.g.  longest *)
(* chain with some deterministic tie breaker), they will eventually agree on  *)
(* a longer chain to finalise in subsequent rounds.                           *)
(*                                                                            *)
(* For byzantine fault-tolerance, the protocol rests on the standard          *)
(* assumption of f < 1/3 byzantine validators.  The safety of the protocol is *)
(* thus based on honest validators observing a supermajority of votes in each *)
(* phase of a round.  Liveness in the partially synchronous network model     *)
(* depends on an appropriate choice for the upper bound of the message        *)
(* transmission time T, which is assumed to hold during periods of synchrony, *)
(* i.e.  after an (unknown) global stabilisation time (GST).                  *)
(*                                                                            *)
(* The protocol presupposes that votes are cryptographically signed and thus  *)
(* canot be forged and a particular validator cannot impersonate one or more  *)
(* other Validators.  The protocol furthermore assumes that messages of all   *)
(* honest Validators are eventually delivered, but may be delivered multiple  *)
(* times or out of order.  This assumption manifests in the monotonically     *)
(* increasing set msgs of sent messages in this TLA+ specification.  To that  *)
(* end, an implementation may need to incorporate bounded retransmission or   *)
(* gossiping of votes among the validators, possibly with application-level   *)
(* acknowledgements, especially if the validators cannot be assumed to form a *)
(* fully connected graph.                                                     *)
(*                                                                            *)
(*                                                                            *)
(* `^\large{\textbf{Accountable Safety}}^'                                    *)
(*                                                                            *)
(* The GRANDPA protocol has the property of accountable safety, i.e.  that if *)
(* f >= 1/3 of Validators are byzantine, then at least 1/3 of these can       *)
(* eventually be identified and "punished", e.g.  by taking away some or all  *)
(* of their stake in a Proof-of-Stake based system.  This property is not     *)
(* reflected in this specification, as the procedure for identifying the      *)
(* byzantine Validators is distinct from the finality consensus algorithm and *)
(* may be specified separately.                                               *)
(*                                                                            *)
(*                                                                            *)
(* `^\large{\textbf{Primary Validator}}^'                                     *)
(*                                                                            *)
(* The GRANDPA protocol relies on the notion of an (arbitrarily chosen)       *)
(* primary among the Validators in a round of the protocol for liveness in    *)
(* the partially synchronous model under consideration of a specific kind of  *)
(* timed, repeated vote splitting attack (see [1], section 6.2).  Since this  *)
(* TLA+ specification is not a specification of a real-time system but only   *)
(* specifies liveness in the form of temporal properties that assert eventual *)
(* progress, the primary is omitted from this specification, with additional  *)
(* steps relating to the primary that an implementation should perform        *)
(* mentioned in the commentary of the relevant actions.                       *)
(*                                                                            *)
(*                                                                            *)
(* `^\large{\textbf{Liveness}}^'                                              *)
(*                                                                            *)
(* The proofs of liveness in the partially synchronous model rely on an       *)
(* appropriate choice for the upper bound of the message transmission time T. *)
(* The lemmas and theorems in [1] then show that the protocol is guaranteed   *)
(* to make progress within specific time bounds that are multiples of T.      *)
(* Roughly, the reasoning behind the protocol execution with intentional      *)
(* delays is the following:                                                   *)
(*                                                                            *)
(*  1. Let t be the time when the first honest validator v starts round r + 1. *)
(*     Since v started a new round, v sees round r as completable.            *)
(*  2. Since round r is completable, all relevant messages have been          *)
(*     sent and all Validators see that round r is completable at the         *)
(*     latest at time t + T.                                                  *)
(*  3. The primary thus also sees round r as completable at the latest        *)
(*     at time t + T and may immediately send the block hint in form of       *)
(*     the previous round estimate, followed by its prevote.                  *)
(*  4. The primary's block hint and prevote for round r is received by        *)
(*     all other Validators until time t + T + T = t + 2T.                    *)
(*  5. Since all non-primary Validators wait 2T before prevoting              *)
(*     in round r, they should have received the primary's block hint,        *)
(*     if any, and can thus incorporate it into their own prevote             *)
(*     (i.e. supporting the primary's block hint).                            *)
(*  6. Since the last validator to send a prevote in round r does so until    *)
(*     t + T + 2T = t + 3T, all prevotes are received until time t + 4T.      *)
(*  7. A validator that waits t + 4T before precommitting can thus assume     *)
(*     to have received all prevotes from honest Validators, even if it       *)
(*     already observed a supermajority of prevotes earlier than t + 4T.      *)
(*                                                                            *)
(* If T is chosen too small, such that many messages don't arrive within T    *)
(* after being sent during periods of synchrony (i.e.  a "healthy" network),  *)
(* the liveness proofs no longer hold.  On the other hand, T induces a lower  *)
(* bound for how fast new blocks can be finalised and thus choosing T         *)
(* unreasonably large results in slow finalisation.                           *)
(*                                                                            *)
(*                                                                            *)
(* `^\large{\textbf{Validator Set Changes}}^'                                 *)
(*                                                                            *)
(* Typically, an implementation of the protocol will need to account for      *)
(* potential validator set changes (between rounds or at certain block        *)
(* heights).  How that is done is beyond the scope of this TLA+               *)
(* specification, which uses a fixed validator set, but some approaches for   *)
(* how such a "view change protocol" can be incorporated are outlined in      *)
(* section 5 of [1] and usually require identification of the voter set (i.e. *)
(* the current "view") in the vote messages.                                  *)
(*                                                                            *)
(*                                                                            *)
(* `^\large{\textbf{Commit Messages}}^'                                       *)
(*                                                                            *)
(* A commit message sent by a validator signals the finalisation of a block,  *)
(* including as justification the observed precommits.  Besides the fact that *)
(* receiving a valid commit message offers an additional finalisation         *)
(* condition, their primary use is in the context of the accountable safety   *)
(* protocol.  Therefore, commit messages are not included in this TLA+        *)
(* specification.                                                             *)
(*                                                                            *)
(*                                                                            *)
(* `^\large{\textbf{Weighted Votes}}^'                                        *)
(*                                                                            *)
(* As far as the protocol is concerned, weighted votes with a total weight of *)
(* N distributed among M < N Validators is equivalent to a setup with N       *)
(* Validators, with the obvious consideration that the voting weight allotted *)
(* to a single validator is in its entirety subject to that validator's       *)
(* behaviour and availability.                                                *)
(*                                                                            *)
(*                                                                            *)
(*                                                                            *)
(* `^\large{\textbf{Persistent Round State}}^'                                *)
(*                                                                            *)
(* An implementation will have to bound the number of past rounds for which   *)
(* the observed votes and associated metadata are kept.  Beyond the previous  *)
(* round, whose observed votes are always needed for correct protocol         *)
(* execution, earlier rounds are primarily needed to participate in the       *)
(* accountable safety protocol, for which there will have to be made a        *)
(* tradeoff w.r.t.  how far back misbehaviours may still be identified and    *)
(* punished.  This TLA+ specification uses a simple model that maintains      *)
(* state for all rounds.                                                      *)
(*                                                                            *)
(*                                                                            *)
(* `^\large{\textbf{Common Abbreviations}}^'                                  *)
(*                                                                            *)
(*   v: `Voter / Validator'                                                   *)
(*   r: `Round number'                                                        *)
(*   b: `Block'                                                               *)
(*   g: `GHOST-block'                                                         *)
(*   t: `Type of vote / phase of a round' (i.e. prevote or precommit)         *)
(*                                                                            *)
(******************************************************************************)
EXTENDS Integers, Sequences, FiniteSets

CONSTANTS Validators, Nil, BestChain(_), BestChainFrom(_,_)
VARIABLES blocks, final, msgs, rounds

vars == <<blocks, final, msgs, rounds>>

N == Cardinality(Validators)
F == (N - 1) \div 3

(******************************************************************************)
(* The minimum number of votes needed for a block to have a supermajority     *)
(* in a set of votes.                                                         *)
(*                                                                            *)
(* A supermajority is a majority that is large enough such that it is         *)
(* guaranteed to contain a majority of votes from honest nodes. Since         *)
(* there can be up to F byzantine voters, the definition                      *)
(* `^\[ \frac{1}{2}(N + F + 1) \]^' follows.                                  *)
(******************************************************************************)
MinSuperMajority ==
    LET n == N + F + 1
    IN (n \div 2) + (n % 2)

(*
A possible alternative definition for the supermajority is N - F.
When F is rational, these definitions are equivalent, since N can then
always be written as 3F + 1 and thus:
`^\[
      \frac{1}{2}(N + F + 1)
    = \frac{1}{2}(4F + 2)
    = 2F + 1
    = 3F + 1 - F
    = N - F
\]^'
However, when F is taken to be the (integer) quotient of the division
`^\[
    (N - 1) \div 3 = \lfloor F \rfloor,
\]^'
i.e. a natural number, then
`^\[
    N - \lfloor F \rfloor \geq \frac{1}{2}(N + \lfloor F \rfloor + 1).
\]^'
Since the remainder of the division in F by 3 can be at most 2, in
which case
`^\[
    F - \lfloor F \rfloor = \frac{2}{3}
\]^'
we have
`^\[
      N - (\lfloor F \rfloor + \frac{2}{3})
    = \frac{1}{2}(N + \lfloor F \rfloor + \frac{2}{3} + 1)
    = \frac{1}{2}(N + \lfloor F \rfloor + 1) + \frac{1}{3}
\]^'
and thus
`^\[
      N - \lfloor F \rfloor
    = \frac{1}{2}(N + \lfloor F \rfloor + 1) + 1
\]^'
meaning that whenever N is a multiple of 3, N - F requires one more
vote for a supermajority than strictly necessary. On the upside, in
these cases safety (though not liveness) of the protocol is retained
in the presence of one more byzantine voter.
*)
MinSuperMajorityAlt == N - F

-------------------------------------------------------------------------------
(******************************************************************************)
(* Instantiate the high-level specifications of block production              *)
(* and finality, including some local re-definitions for convenience.         *)
(******************************************************************************)

BP == INSTANCE BlockProduction WITH Nodes <- Validators

b1 \preceq b2 == BP!\preceq(b1, b2) 
b1 \prec b2 == BP!\prec(b1, b2)
b1 \succeq b2 == BP!\succeq(b1, b2)
b1 \succ b2 == BP!\succ(b1, b2)

Genesis == BP!Genesis
Block == BP!Block
Parent(b) == BP!Parent(b)
Children(v, b) == BP!Children(v, b)
Ancestors(b) == BP!Ancestors(b)
Last(s) == BP!Last(s)

FIN == INSTANCE Finality

--------------------------------------------------------------------------------
(******************************************************************************)
(* Data definitions.                                                          *)
(******************************************************************************)

RoundNr == Nat \ {0}

Vote ==
    [ type   : {"prevote", "precommit"}
    , round  : RoundNr
    , block  : Block
    , sender : Validators
    ]

Round ==
    [ current : RoundNr
    , votes   : Seq(SUBSET Vote)
    ]

-------------------------------------------------------------------------------
(******************************************************************************)
(* Preliminaries                                                              *)
(******************************************************************************)

PrevoteSent(v, r) ==
    \E m \in msgs:
        m.type = "prevote" /\ m.round = r /\ m.sender = v

PrecommitSent(v, r) ==
    \E m \in msgs:
        m.type = "precommit" /\ m.round = r /\ m.sender = v

RECURSIVE AddVote(_,_)
AddVote(m, ms) ==
    CASE m.round <= Len(ms) -> [ms EXCEPT ![m.round] = @ \cup {m}]
      [] m.round >  Len(ms) ->
          LET tail == [i \in 1 .. m.round - Len(ms) |-> {}]
          IN AddVote(m, ms \o tail)

IsFinal(v, b) == b \preceq Last(final[v])

-------------------------------------------------------------------------------
(******************************************************************************)
(* Equivocation, support and supermajority for sets                           *)
(* of (observed) votes.                                                       *)
(******************************************************************************)

(******************************************************************************)
(* Whether v is an equivocator in phase t of round r in the set of votes s.   *)
(* An equivocator is a validator for which s contains multiple votes          *)
(* for different blocks.                                                      *)
(******************************************************************************)
IsEquivocator(v, r, t, s) ==
    \E m1, m2 \in s:
        /\ m1.type = t /\ m1.sender = v /\ m1.round = r
        /\ m2.type = t /\ m2.sender = v /\ m2.round = r
        /\ m1.block # m2.block

(******************************************************************************)
(* The set of equivocators in phase t of round r as seen by validator v.      *)
(******************************************************************************)
Equivocators(v, r, t) ==
    LET s == rounds[v].votes[r]
    IN {w \in Validators: IsEquivocator(w, r, t, s)}

(******************************************************************************)
(* The set of non-equivocating supporters of block b in phase t of round r,   *)
(* as seen by validator v.                                                    *)
(*                                                                            *)
(* For b = Nil, this is defined to be the set of all non-equivocating         *)
(* voters in phase t of round r, as seen by validator v.                      *)
(******************************************************************************)
Supporters(v, r, t, b) ==
    LET s == rounds[v].votes[r]
        u == { m \in s:
                /\ m.round = r
                /\ m.type = t
                /\ (b = Nil \/ b \preceq m.block)
                /\ ~ IsEquivocator(m.sender, r, t, s)
             }
    IN {m.sender: m \in u}

(******************************************************************************)
(* Whether v sees a supermajority of votes for a block b in the set of        *)
(* observed votes in phase t of round r.                                      *)
(*                                                                            *)
(* A supermajority of votes for a block b guarantees that there is a majority *)
(* of votes from honest Validators for b, since a supermajority is always at  *)
(* least of size 2F + 1, of which F may be byzantine, leaving a majority of   *)
(* at least F + 1 honest votes for b.                                         *)
(*                                                                            *)
(* Equivocations count as votes for any block, so that the property of having *)
(* a supermajority is monotonic w.r.t.  a set of observed votes, i.e.  if S   *)
(* and T are sets of votes, S \subseteq T and there is a supermajority for    *)
(* block b in S, then there is a supermajority for block b in T.              *)
(******************************************************************************)
SuperMajority(v, r, t, b) ==
    LET
    p == Cardinality(Supporters(v, r, t, b))
    q == Cardinality(Equivocators(v, r, t))
    IN
    p + q >= MinSuperMajority

(******************************************************************************)
(* Whether v sees that it is possible for a block b to have supermajority in  *)
(* the set of observed votes in phase t of round r.  This is the case iff the *)
(* number of voters who either equivocate or vote for a different block (i.e. *)
(* not for b or a descendant of b) is less than MinSuperMajority.             *)
(*                                                                            *)
(* Note that it is still possible for a block to obtain a supermajority even  *)
(* if half of the Validators already voted for a different block due to the   *)
(* possibility of equivocations, e.g.  with N = 4 and 2 Validators already    *)
(* voted for a block b' # b, b may still get supermajority if it turns out    *)
(* that one of these Validators equivocates and the remaining 2 Validators    *)
(* vote for b.                                                                *)
(*                                                                            *)
(* Note further that as per this definition and the definition of             *)
(* SuperMajority, a block b may have a supermajority in a set of votes while  *)
(* it is at the same time impossible for b to have such a supermajority, if   *)
(* there are more than F equivocators.  Such a set of votes is called         *)
(* intolerant and violates the assumption of there being at most F byzantine  *)
(* Validators, which is essential for the safety of the protocol.  Thus       *)
(* SuperMajority and PossibleSuperMajority are only consistent with each      *)
(* other for tolerant sets of votes.                                          *)
(******************************************************************************)
PossibleSuperMajority(v, r, t, b) ==
    LET
    p == Cardinality(Supporters(v, r, t, b))
    q == Cardinality(Equivocators(v, r, t))
    a == Cardinality(Supporters(v, r, t, Nil))
    IN
    CASE N <  3 -> a = p
      [] N >= 3 -> (a - p) + q < MinSuperMajority

-------------------------------------------------------------------------------
(******************************************************************************)
(* Definitions for chain selection, round estimates and completability of     *)
(* a round based on the concept of supermajorities.                           *)
(******************************************************************************)

(******************************************************************************)
(* The "2/3-GHOST" Ghost(v, r, t) is defined to be the block b with           *)
(* highest block number having a supermajority of votes in                    *)
(* the set of votes observed by v in phase t of round r.  If there is no      *)
(* block  with a supermajority, the 2/3-GHOST is defined to be Nil.           *)
(*                                                                            *)
(* If it exists, Ghost(v, r, t) is uniquely defined, since there can be no    *)
(* two different blocks at the same block height (nr) both having a           *)
(* supermajority of votes.  Furthermore, due to a vote on a block b being a   *)
(* vote on all blocks on the chain with head b, the 2/3-GHOST is              *)
(* monotonically increasing w.r.t.  \preceq as more votes are observed.       *)
(*                                                                            *)
(* Since all blocks being voted on have a common ancestor (at least the       *)
(* Genesis block), Ghost(v, r, t) # Nil iff votes from a supermajority        *)
(* of voters have been observed by v (in phase t of round r).                 *)
(******************************************************************************)
Ghost(v, r, t) ==
    CASE r = 0 -> Nil
      [] r > 0 ->
          LET bs == {b \in blocks[v]: SuperMajority(v, r, t, b)}
          IN
          IF bs = {} THEN Nil ELSE BP!MaxBlockByNr(bs)

(******************************************************************************)
(* The estimate of validator v of the largest block (w.r.t \preceq) that may  *)
(* be finalised in round r, given by the last block in the chain with head    *)
(* Ghost(v, r, "prevote") for which it is still possible for the precommits   *)
(* in round r to have a supermajority.                                        *)
(*                                                                            *)
(* If the estimate does not exist, it is defined to be Nil.  An estimate for  *)
(* round r exists iff a prevote-ghost exists, i.e.  Estimate(v, r) # Nil      *)
(* `^\iff^' Ghost(v, r, "prevote") # Nil.                                     *)
(*                                                                            *)
(* Note: If Estimate(v, r) = Ghost(v, r, "prevote") == g, it may still be     *)
(* possible for a child of g to achieve a supermajority of precommits, i.e.   *)
(* the estimate can still move up or down the chain as more votes are         *)
(* observed.  See the definition of Completable(v, r) for when the estimate   *)
(* becomes monotonically decreasing w.r.t.  \preceq.                          *)
(******************************************************************************)
Estimate(v, r) ==
    LET
    g == Ghost(v, r, "prevote")
    check[b \in Block] ==
        IF PossibleSuperMajority(v, r, "precommit", b)
        THEN b
        ELSE check[Parent(b)]
    IN
    CASE r = 0   -> Genesis
      [] g = Nil -> Nil
      [] OTHER   -> check[g]

(******************************************************************************)
(* Whether validator v sees round r as completable, allowing it to progress   *)
(* to the next round.                                                         *)
(*                                                                            *)
(* Let g == Ghost(v, r, "prevote") # Nil.  Then validator v sees round r as   *)
(* completable if either:                                                     *)
(*                                                                            *)
(* a) Estimate(v, r) \prec g, since it is then impossible for any descendant  *)
(* of Estimate(v, r), including g and any child of g, to have a supermajority *)
(* of precommits,                                                             *)
(*                                                                            *)
(* or                                                                         *)
(*                                                                            *)
(* b) Estimate(v, r) = g (by definition of Estimate) and it is impossible for *)
(* the precommits in round r to have a supermajority for any children of g.   *)
(* It is impossible for any child of g to have a supermajority of precommits  *)
(* if v observed precommits from a supermajority of voters in round r (thus g *)
(* already has a supermajority of precommits) and it is impossible for any    *)
(* child of g to have supermajority.  It is thereby assumed that v has any    *)
(* blocks for which it received votes in its block tree, requesting them from *)
(* a peer if necessary.  Thus it is also impossible for any child of g that v *)
(* has not yet seen to have a supermajoority of precommits.                   *)
(*                                                                            *)
(* The property of completability is monotonic in the set of observed votes   *)
(* in round r, i.e.  for observed votes S \subset S' in round r, r being      *)
(* completable w.r.t.  S implies r is completable w.r.t.  S'.  Thus, by       *)
(* definition, if round r is completable there exists a round estimate for r  *)
(* that is monotonically decreasing w.r.t.  \preceq.                          *)
(******************************************************************************)
Completable(v, r) ==
    LET
    s == {m \in rounds[v].votes[r]: m.type = "precommit"}
    g == Ghost(v, r, "prevote")
    e == Estimate(v, r)
    IN
    CASE g = Nil -> FALSE
      [] r > Len(rounds[v].votes) -> FALSE
      [] OTHER ->
          \/ e \prec g
          \/ /\ Cardinality({m.sender: m \in s}) >= MinSuperMajority
             /\ \A b \in Children(v, g):
                 ~ PossibleSuperMajority(v, r, "precommit", b)

-------------------------------------------------------------------------------
(******************************************************************************)
(* The protocol actions / next state relations.                               *)
(******************************************************************************)

(******************************************************************************)
(* Produce a new block.  This step abstracts over the concurrent block        *)
(* production algorithm that provides the input for the finality protocol.    *)
(*                                                                            *)
(* This action is not part of the GRANDPA protocol, but of the underlying     *)
(* block production and distribution protocol.                                *)
(*                                                                            *)
(* To somewhat limit the state space, each validator is allowed to produce    *)
(* only one new block in each round.                                          *)
(******************************************************************************)
NewBlock(v) ==
    /\ Cardinality({c \in blocks[v]: c.author = v}) < rounds[v].current 
    /\ BP!NewBlock(v)
    /\ UNCHANGED <<msgs, final, rounds>>

(******************************************************************************)
(* "Receive" a block from another validator.                                  *)
(*                                                                            *)
(* This action is not part of the GRANDPA protocol, but of the underlying     *)
(* block production and distribution protocol.                                *)
(******************************************************************************)
RecvBlock(v) ==
    /\ BP!RecvBlock(v)
    /\ UNCHANGED <<msgs, final, rounds>>

(******************************************************************************)
(* Receive a message.                                                         *)
(*                                                                            *)
(* Receiving a vote for a block is understood to imply obtaining the chain of *)
(* the block being voted on, if necessary.  The missing blocks of the chain   *)
(* may thus need to be fetched through a separate synchronisation protocol in *)
(* an implementation.                                                         *)
(******************************************************************************)
RecvVote(v) ==
    \E m \in msgs:
        /\ CASE
            m.round  > rounds[v].current -> TRUE []
            m.round <= rounds[v].current ->
                ~ \E n \in rounds[v].votes[m.round]: m = n
        /\ rounds' = [rounds EXCEPT ![v].votes = AddVote(m, @)]
        /\ blocks' = [blocks EXCEPT ![v] = @ \cup Ancestors(m.block)]
        /\ UNCHANGED <<msgs, final>>

(******************************************************************************)
(* Prevote for a block.                                                       *)
(*                                                                            *)
(* If v did not yet prevote for a block in round r, it sends a prevote as     *)
(* follows:                                                                   *)
(*                                                                            *)
(* (i) If v has recieved a block b from the primary, v prevotes for the head  *)
(* of the best chain containing b as soon as one of the following holds:      *)
(*                                                                            *)
(*   (a) Ghost(v, r - 1, "prevote") \succeq b \succeq Estimate(v, r - 1)      *)
(*   (b) The best chain containing b is also the best chain containing        *)
(*       Estimate(v, r - 1).                                                  *)
(*                                                                            *)
(* (ii) If round r is completable and Estimate(v, r) \succeq Estimate(v, r -  *)
(* 1), then v prevotes for Estimate(v, r).                                    *)
(*                                                                            *)
(* (iii) If v reached time t + 2T and it did not receive a message from the   *)
(* primary or (i)(a) does not hold, then v prevotes for the head of best      *)
(* chain containing Estimate(v, r - 1) anyway.                                *)
(*                                                                            *)
(* Thereby (i) and (ii) identify conditions that allow a validator to prevote *)
(* before time t + 2T.  Conditions (i)(a) and (i)(b) ensure that a byzantine  *)
(* primary cannot steer votes towards a block that could cause a safety       *)
(* violation down the road, but merely assists in establishing agreement      *)
(* among honest voters in the presence of vote splitting attacks on the       *)
(* liveness of the protocol.  Since this TLA+ specification omits the primary *)
(* and does not track elapsed real time, condition (iii) is always satisfied. *)
(*                                                                            *)
(* Compared to the algorithm in [1], this action has the additional           *)
(* precondition that the head of the best chain containing Estimate(v, r - 1) *)
(* must not already be final, which avoids running superfluous rounds.        *)
(******************************************************************************)
Prevote(v) ==
    LET
    r == rounds[v].current
    b == BestChainFrom(v, Estimate(v, r - 1))
    m == [ round  |-> r
         , type   |-> "prevote"
         , block  |-> b
         , sender |-> v
         ]
    IN
    /\ ~ IsFinal(v, b)
    /\ ~ PrevoteSent(v, r)
    /\ msgs' = msgs \cup {m}
    /\ rounds' = [rounds EXCEPT ![v].votes[r] = @ \cup {m}]
    /\ UNCHANGED <<blocks, final>>

(******************************************************************************)
(* Precommit on a block.                                                      *)
(*                                                                            *)
(* If v sees a block with a supermajority of prevotes that is equal to or     *)
(* a descendant of the previous round estimate, i.e.                          *)
(*                                                                            *)
(* g == Ghost(v, r, "prevote") \succeq Estimate(v, r - 1)                     *)
(*                                                                            *)
(* and either                                                                 *)
(*                                                                            *)
(*   (i) round r is completable, or                                           *)
(*   (ii) v has seen a child of the last finalised block and it is impossible *)
(*         for the set of prevotes to obtain supermajority for any child of   *)
(*         g, or                                                              *)
(*   (iii) v has seen a child of the last finalised block and the time is     *)
(*         at least t + 4T                                                    *)
(*                                                                            *)
(* then v precommits on g.                                                    *)
(*                                                                            *)
(* Thereby conditions (i) and (ii) identify situations that allow a validator *)
(* to precommit before time t + 4T. Since this TLA+ specification has no      *)
(* notion of elapsed wall-clock time, t + 4T is always "elapsed" and only     *)
(* conditions (i) and (iii) are used here.                                    *)
(******************************************************************************)
Precommit(v) ==
    LET
    r == rounds[v].current
    g == Ghost(v, r, "prevote")
    e == Estimate(v, r - 1)
    IN
    CASE g = Nil -> FALSE
        [] OTHER ->
            /\ PrevoteSent(v, r)
            /\ ~ PrecommitSent(v, r)
            /\ g \succeq e
            /\ \/ Completable(v, r)
               \/ \E c \in blocks[v]: c \succ Last(final[v])
            /\ LET
                m == [ round  |-> r
                     , type   |-> "precommit"
                     , block  |-> g
                     , sender |-> v
                     ]
                IN
                /\ msgs' = msgs \cup {m}
                /\ rounds' = [rounds EXCEPT ![v].votes[r] = @ \cup {m}]
                /\ UNCHANGED <<blocks, final>>

(******************************************************************************)
(* Block finalisation.                                                        *)
(*                                                                            *)
(* If, for some round r, at any point after the precommit step of round r, v  *)
(* sees that g == Ghost(v, r, "precommit") is a descendant of the last        *)
(* finalised block and there is a supermajority of prevotes for g in round r, *)
(* v finalises g.                                                             *)
(******************************************************************************)
Finalise(v) ==
    \E r \in DOMAIN rounds[v].votes:
        LET
        g == Ghost(v, r, "precommit")
        c == Tail(BP!Chain(Last(final[v]), g))
        IN
        CASE g = Nil -> FALSE
            [] OTHER ->
                /\ Last(final[v]) \prec g
                /\ SuperMajority(v, r, "prevote", g) \* Really necessary?
                /\ final' = [final EXCEPT ![v] = @ \o c]
                /\ UNCHANGED <<msgs, rounds, blocks>>

(******************************************************************************)
(* Transition to the next round of voting.                                    *)
(*                                                                            *)
(* v can start the next round (r + 1) when it sees the current round r as     *)
(* completable, Estimate(v, r - 1) is finalised and v has cast votes in all   *)
(* previous rounds.                                                           *)
(*                                                                            *)
(* The precondition that Estimate(v, r - 1) is finalised allows a validator   *)
(* to only keep track of the previous round state (except as required for     *)
(* participation in the accountable safety protocol). If Estimate(v, r - 1)   *)
(* was not already finalised in the previous round (r - 1), it is guaranteed  *)
(* to be finalised in the current round because all prevotes of honest        *)
(* Validators must be on a block equal to (or descending from) the last       *)
(* round estimate, which is thus guaranteed to obtain a supermajority of      *)
(* prevotes and precommits and thus be finalisable in round r.                *)
(*                                                                            *)
(* The precondition ~ ENABLED Finalise(v) establishes that all blocks         *)
(* that can be finalised should be finalised before entering the next round.  *)
(* While not a safety requirement, it avoids situations where a prevote is    *)
(* sent for the same block in subsequent rounds, even though the earlier      *)
(* round may already result in the block being finalisable.                   *)
(******************************************************************************)
NextRound(v) ==
    /\ ~ ENABLED Finalise(v)
    /\ Completable(v, rounds[v].current)
    /\ IsFinal(v, Estimate(v, rounds[v].current - 1))
    /\ \A r \in DOMAIN rounds[v].votes: PrecommitSent(v, r)
    /\ rounds' = [rounds EXCEPT ![v].votes = Append(@, {}),
                                ![v].current = @ + 1]
    /\ UNCHANGED <<msgs, blocks, final>>

(******************************************************************************)
(* To "jump" to a round r, v needs to see that round r - 1 is completable     *)
(* and that Estimate(v, r - 2) is finalised.    This allows a validator who   *)
(* is "lagging behind" (e.g.  due to being offline) to engage in the latest   *)
(* round after being an observer for two rounds, without having to go through *)
(* the sending of prevote and precommit messages for all earlier rounds.      *)
(******************************************************************************)
JumpToRound(v) ==
    \E r \in DOMAIN rounds[v].votes:
        /\ r > rounds[v].current
        /\ Completable(v, r - 1)
        /\ IsFinal(v, Estimate(v, r - 2))
        /\ rounds' = [rounds EXCEPT ![v].current = r]
        /\ UNCHANGED <<msgs, blocks, final>>

\* TODO: Action Equivocate(v) that simulates byzantine behaviour,
\* possibly in a separate module, since it is of course not to be implemented.

-------------------------------------------------------------------------------
(******************************************************************************)
(* The (safety) specification.                                                *)
(******************************************************************************)

Init ==
    /\ rounds = [v \in Validators |-> [current |-> 1, votes |-> <<{}>>]]
    /\ blocks = [v \in Validators |-> {Genesis}]
    /\ final  = [v \in Validators |-> <<Genesis>>]
    /\ msgs   = {}

Next ==
    \E v \in Validators:
        \/ NewBlock(v)
        \/ RecvBlock(v)
        \/ NextRound(v)
        \/ Prevote(v)
        \/ Precommit(v)
        \/ RecvVote(v)
        \/ Finalise(v)
        \/ JumpToRound(v)

Spec == Init /\ [][Next]_vars

(******************************************************************************)
(* The type invariant of the safety specification.                            *)
(******************************************************************************)
TypeOK ==
    /\ msgs \subseteq Vote
    /\ rounds \in [Validators -> Round]
    /\ blocks \in [Validators -> SUBSET Block]
    /\ final \in [Validators -> Seq(Block)]

THEOREM Spec => []TypeOK

(******************************************************************************)
(* GRANDPA implements / refines the high-level (safety) specification of a    *)
(* "finality gadget", meaning that every model of GRANDPA's `Spec' (i.e.      *)
(* every behaviour satisfying `Spec') is also a model of `FIN!Spec'.          *)
(******************************************************************************)
THEOREM Spec => FIN!Spec

\* TODO: TLAPS proofs

-------------------------------------------------------------------------------
(******************************************************************************)
(* The liveness specification.                                                *)
(******************************************************************************)
LiveSpec == Spec /\ WF_vars(Next)

THEOREM LiveSpec => FIN!LiveSpec

===============================================================================